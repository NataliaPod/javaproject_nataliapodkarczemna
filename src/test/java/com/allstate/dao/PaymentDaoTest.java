package com.allstate.dao;

import com.allstate.entities.Payment;
import org.junit.After;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;

import static org.junit.Assert.assertEquals;

import java.util.Date;
import java.util.List;

@SpringBootTest
public class PaymentDaoTest {

    @Autowired
    IPaymentDao dao;

    @Autowired
    MongoTemplate mongoTemplate;

    @BeforeEach
    @AfterEach
    public void cleanUp() {
        for (String collectionName : mongoTemplate.getCollectionNames()) {
            if (!collectionName.startsWith("system.")) {
                mongoTemplate.dropCollection(collectionName);
            }
        }
    }

    @Test
    public void testSavePayment(){
        Payment payment = new Payment(1, new Date(2020,9,10), "cash", 10.00, 231 );
        dao.save(payment);

    }

    @Test
    public void findPaymentByID() {
        Payment payment = new Payment(3, new Date(2020,9,10), "cash", 10.00, 2);
        dao.save(payment);
        dao.findById(3);
    }

    @Test
    public void findPaymentByType() {
        Payment payment = new Payment(1, new Date(2020,9,10), "cash", 10.00, 2);
        dao.save(payment);
        Payment payment2 = new Payment(2, new Date(2020,9,10), "transfer", 10.00, 2);
        dao.save(payment2);
        List<Payment> paymentList = dao.findByType("cash");
    }

    @Test
    public void countRowsOfPayment() {
        Payment payment = new Payment(1, new Date(2020,9,10), "cash", 10.00, 2);
        dao.save(payment);
        Payment payment2 = new Payment(2, new Date(2020,9,10), "transfer", 10.00, 2);
        dao.save(payment2);
        Payment payment3 = new Payment(3, new Date(2020,9,10), "transfer", 10.00, 2);
        dao.save(payment3);
        int count = dao.rowCount();
        assertEquals(3, count );
    }
}
