package com.allstate.service;

import com.allstate.entities.Payment;
import com.allstate.services.PaymentService;
import com.mongodb.internal.thread.DaemonThreadFactory;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;

@SpringBootTest
public class PaymentServiceTest {

    @Autowired
    private PaymentService service;
    @Autowired
    MongoTemplate mongoTemplate;

    @BeforeEach
    // @AfterEach
    public void cleanUp() {
        for (String collectionName : mongoTemplate.getCollectionNames()) {
            if (!collectionName.startsWith("system.")) {
                mongoTemplate.dropCollection(collectionName);
            }
        }
    }

    @Test
    public void countPayments() {
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        Date date = new Date();
        Payment payment = new Payment(1, date, "cash", 10.00, 1);
        service.save(payment);
        int count = service.rowCount();
        assertEquals(1 , count);
    }

    @Test
    public void findPaymentById(){
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        Date date = new Date();
        Payment payment = new Payment(1, date, "cash", 10.00, 1);
        service.save(payment);
        payment =service.findById(1);
        Assert.assertNotNull("Employee found" , payment);
    }

    @Test
    public void findPaymentByType() {
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        Date date = new Date();
        Payment payment1 = new Payment(1, date, "cash", 10.00, 1);
        service.save(payment1);
        Payment payment2 = new Payment(2, date, "check", 10.00, 1);
        service.save(payment2);
        Payment payment3 = new Payment(3, date, "cash", 10.00, 1);
        service.save(payment3);
        List<Payment> paymentList = service.findByType("cash");
        assertEquals(2, paymentList.size());
    }

}
