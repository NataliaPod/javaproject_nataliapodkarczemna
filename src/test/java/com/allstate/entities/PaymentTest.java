package com.allstate.entities;

import org.junit.jupiter.api.Test;


import java.util.Date;

public class PaymentTest {

    @Test
    public void testConstructor(){

        Payment payment = new Payment();
        payment.setId(1);
        payment.setCusId(1);
        payment.setAmount(100);
        payment.setPaymentDate(new Date(2020,9,10));
        payment.setType("cash");
    }
}
