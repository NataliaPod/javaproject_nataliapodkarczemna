package com.allstate.dao;

import com.allstate.entities.Payment;

import java.util.List;

public interface IPaymentDao {
    Integer rowCount();
    Payment findById(int id);
    List<Payment> findByType(String type);
    void save(Payment payment);

}
