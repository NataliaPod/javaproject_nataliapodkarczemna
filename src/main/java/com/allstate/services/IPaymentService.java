package com.allstate.services;

import com.allstate.entities.Payment;

import java.util.List;

public interface IPaymentService {
    Integer rowCount();
    Payment findById(int id);
    List<Payment> findByType(String type);
    void save(Payment payment);
}
