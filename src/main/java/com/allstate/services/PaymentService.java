package com.allstate.services;

import com.allstate.dao.IPaymentDao;
import com.allstate.entities.Payment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PaymentService implements IPaymentService{

    @Autowired
    IPaymentDao dao;

    @Override
    public Integer rowCount() {
        return dao.rowCount();
    }

    @Override
    public Payment findById(int id) {
        if (id > 0) {
            return dao.findById(id);
        } else {
            return null;
        }
    }

    @Override
    public List<Payment> findByType(String type) {
        if (type != null) {
            return dao.findByType(type);
        }
        else {
            return null;
        }
    }

    @Override
    public void save(Payment payment) {
    dao.save(payment);
    }
}
