package com.allstate.entities;

import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Document
public class Payment {

    private int id;
    private Date paymentDate;
    private String type;
    private double amount;
    private int cusId;

    public Payment(int id, Date paymentDate, String type, double amount, int cusId) {
        this.id = id;
        this.paymentDate = paymentDate;
        this.type = type;
        this.amount = amount;
        this.cusId = cusId;
    }

    public Payment() {}

    public int getId() {
        return id;
    }

    @Override
    public String toString() {
        return "Payment{" +
                "id=" + id +
                ", paymentDate=" + paymentDate +
                ", type='" + type + '\'' +
                ", amount=" + amount +
                ", cusId=" + cusId +
                '}';
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentDate(Date paymentDate) {
        this.paymentDate = paymentDate;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public int getCusId() {
        return cusId;
    }

    public void setCusId(int cusId) {
        this.cusId = cusId;
    }
}
