package com.allstate.rest;

import com.allstate.entities.Payment;
import com.allstate.services.IPaymentService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.ListableBeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class PaymentController {
    private static final Logger LOGGER = LoggerFactory.getLogger(PaymentController.class);


    @Autowired
    IPaymentService service;

    @RequestMapping(value = "/status", method = RequestMethod.GET)
    public String status(){
        LOGGER.info("The API is running");
        return "Rest Api is running" ;}

    @RequestMapping(value = "/rowCount", method = RequestMethod.GET)
    public Integer rowCount(){
        LOGGER.info("In the row count");
        return service.rowCount(); }

    @RequestMapping(value = "/findById/{id}", method = RequestMethod.GET)
    public Payment findById(@PathVariable("id") int id)
    {
        LOGGER.info("Finding by id");
        return service.findById(id);
    }

    @RequestMapping(value = "/findByType/{type}", method = RequestMethod.GET)
    public List<Payment> findByType(@PathVariable("type") String type){
        return service.findByType(type);
    }

    @RequestMapping(value = "/save", method =  RequestMethod.POST)
    public  void save(@RequestBody Payment payment){
        service.save(payment);
    }
}
